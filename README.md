# libACE hook

Made to get Rogueamobea's Audio Capture Engine back up on 10.14 for Discord.

## How to use
1. Get Airfoil/Audiohijack or any other ACE using app (can be demo) from rouge... site
1. Grab ACE.framework & Protein.framework from the contents of the package under the Frameworks folder
1. Put those two in user folder under Library/Application Support/discord/<version, e.g. 0.0.282>/modules/discord_voice/
1. Clone project
1. Build with xcode or xcodebuild
1. Copy the resulting libace.dylib to the previously mentioned discord_voice dir (replace the one they provide)
1. Patch discord_voice.node

## Patchind discord_voice.node
1. Open up in your favorite disas (ida/ghidra... wa'ever)
1. Find function `isPlatformVersionAtLeast`
1. Find references to function... hopefully there's one with it called from like `discord::media::soundshare::GetAceInstallStatus(void)`
1. It requires 1.10.15 (10.15), find `mov edx, 0Fh` change to like `mov edx, 0Eh` for 10.14, possibly lower ones if you can get it working lol
1. ...
1. PROFIT!!!

If you actually suceeded with this crap then big clap! You will now be able to use discord screenshare with sound on 10.14

