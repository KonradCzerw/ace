//
//  ace.m
//  ace
//
//  Created by konrad on 2/11/23.
//  2023 NULLderef. No rights reserved.
//  "just go steal it or smth... idc."
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <dlfcn.h>
#import <Cocoa/Cocoa.h>

#define LIBACE_VERSION_EXPECTED @"11.9.2"

@interface ECAudioSourceProcess : NSObject
- (ECAudioSourceProcess*)initWithBundleID:(NSString*)bundleID processName:(NSString*)processName;
- (void)setHierarchal:(bool)hierarchal;
@end

@interface ECAudioDevice : NSObject
- (void)setDeviceName:(NSString*)deviceName;
- (void)setDeviceUID:(NSString*)deviceUID;
- (void)setHidden:(bool)hidden;
- (void)setMutingMode:(NSString*)mutingMode;
- (void)setRate:(double)rate;
- (void)setSource:(ECAudioSourceProcess*)sourceProcess;
- (NSString*)deviceUID;
@end

@interface ECManager : NSObject
+ (ECManager*)sharedManager;
- (NSString*)createAudioDevice:(ECAudioDevice*)audioDevice;
- (NSString*)deleteAudioDevice:(ECAudioDevice*)audioDevice;
@end

static Class ECAudioSourceProcessClass = nil;
static Class ECAudioDeviceClass = nil;
static Class ECManagerClass = nil;

static bool librariesLoaded = false;

static NSMutableDictionary* audioDevices = nil;

NSString* ACEGetInstalledVersion() {
    return LIBACE_VERSION_EXPECTED;
}

NSString* ACEGetInstallableVersion() {
    return LIBACE_VERSION_EXPECTED;
}

bool ACEGetInstalledVersionAuthorized() {
    return true;
}

NSString* findDiscordVoicePath() {
    // god forbid me for what i have done
    return [[[[[[[NSURL fileURLWithPath:[[[NSProcessInfo processInfo] environment] objectForKey:@"DISCORD_PRELOAD"]] URLByDeletingLastPathComponent] URLByDeletingLastPathComponent] URLByDeletingLastPathComponent] URLByDeletingLastPathComponent] URLByAppendingPathComponent:@"discord_voice"] path];
}

NSString* createDeviceUID() {
    NSString* bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    return [NSString stringWithFormat:@"ACE:%@:%@", [[NSUUID UUID] UUIDString], [bundleIdentifier length] ? bundleIdentifier : @"com.rogueamoeba.ACE"];
}

// first "full" function call, grab our real ace lib with dlopen
NSString* ACECreateApplicationDevice(NSString* bundleID) {
    if(!librariesLoaded) {
        NSString* discordVoicePath = findDiscordVoicePath();
        
        dlopen([[NSString stringWithFormat:@"%@/Protein.framework/Protein", discordVoicePath] UTF8String], RTLD_LAZY);
        dlopen([[NSString stringWithFormat:@"%@/ACE.framework/ACE", discordVoicePath] UTF8String], RTLD_LAZY);
        
        ECAudioSourceProcessClass = NSClassFromString(@"ECAudioSourceProcess");
        ECAudioDeviceClass = NSClassFromString(@"ECAudioDevice");
        ECManagerClass = NSClassFromString(@"ECManager");
        
        audioDevices = [[NSMutableDictionary alloc] init];
        
        librariesLoaded = true;
    }
    
    ECAudioSourceProcess* sourceProcess = [[ECAudioSourceProcessClass alloc] initWithBundleID:bundleID processName:nil];
    [sourceProcess setHierarchal:true];
    
    ECAudioDevice* audioDevice = [[ECAudioDeviceClass alloc] init];
    [audioDevice setDeviceName:bundleID];
    [audioDevice setDeviceUID:createDeviceUID()];
    [audioDevice setHidden:true];
    [audioDevice setMutingMode:@"never"];
    [audioDevice setRate:0];
    [audioDevice setSource:sourceProcess];
    
    [[ECManagerClass sharedManager] createAudioDevice:audioDevice];
    
    NSString* deviceUID = [audioDevice deviceUID];
    [audioDevices setObject:audioDevice forKey:deviceUID];
    
    return deviceUID;
}

void ACEDeleteDevice(NSString* deviceUID) {
    [[ECManagerClass sharedManager] deleteAudioDevice:[audioDevices objectForKey:deviceUID]];
    [audioDevices removeObjectForKey:deviceUID];
}
